# Technical Interview

2 hour tech test - get it working was desirable result then discussion about design, testing.

My approach:
- get APIs integrated with Feign: added testing-support endpoint, tested with Postman
- add business logic to return /weather response as defined in brief

My testing approach would be:

- unit tests
- integration tests: spring boot test - mocking external apis with wiremock
- functional tests: full integration of full deployments - staging APIs probably

# Brief

## Weather App

We would like to add a widget on the site that shows the forecast for given cities.

We need to implement an endpoint that, given a list of cities, returns a list of forecast for those cities:

`/weather?city=los%20angels,%20new%20york,chicago`

For each single city we we would like to know the daily forecast plus the forecasts of the next 2 days

Example:

```json
{
  "forecast": [
    {
      "name": "Los Angeles",
      "detail": [
        {
          "startTime": "2019-07-30T16:40:53.256",
          "endTime": "2019-07-30T16:40:53.255",
          "description": "Mostly sunny, with a high near 84. Northeast wind around 7 mph."
        },
        {
          "startTime": "2019-07-31T16:40:53.256",
          "endTime": "2019-07-31T16:40:53.256",
          "description": "Partly cloudy, with a low around 72."
        },
        {
          "startTime": "2019-08-01T16:40:53.256",
          "endTime": "2019-08-01T16:40:53.256",
          "description": "A chance of showers and thunderstorms. Mostly sunny, with a high near 83."
        }
      ]
    },
    {
      "name": "New York",
      "detail": [
        {
          "startTime": "2019-07-30T16:40:53.256",
          "endTime": "2019-07-30T16:40:53.255",
          "description": "Mostly sunny, with a high near 84. Northeast wind around 7 mph."
        },
        {
          "startTime": "2019-07-31T16:40:53.256",
          "endTime": "2019-07-31T16:40:53.256",
          "description": "Partly cloudy, with a low around 72."
        },
        {
          "startTime": "2019-08-01T16:40:53.256",
          "endTime": "2019-08-01T16:40:53.256",
          "description": "A chance of showers and thunderstorms. Mostly sunny, with a high near 83."
        }
      ]
    }
  ]
}
```
## Hints:
 - in order to convert a string (New York) in a pair of coordinates (40.71427, -74.00597) you can use this service https://nominatim.openstreetmap.org/search?q=new%20york,usa&format=json
 - in order to get the forecast of a pair of coordinates you can use this service https://api.weather.gov/points/40.71427,-74.00597
 - The domain objects have been already created 
 

 ## What we would like to see
 - tiny service that works just fine (and we can launch it via command line `./gradlew clean test bootRun`)
 - thoughtful design that can be tested
 - (bonus) implement a file based cache system without using any framework
