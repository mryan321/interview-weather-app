package com.mryan.weather.service;

import com.mryan.weather.domain.ForecastResponse;
import com.mryan.weather.domain.City;
import com.mryan.weather.forecast.ForecastResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ForecastService {

    @Autowired
    private ForecastResponseBuilder forecastResponseBuilder;

    @Autowired
    private CoordinateService coordinateService;

    public ForecastResponse getForecast(List<String> usCities) {
        List<City> citys = usCities.stream()
            .map(cityQuery -> coordinateService.getCoordinates(cityQuery))
            .collect(Collectors.toList());
        return forecastResponseBuilder.getForecastResponse(citys);
    }

}
