package com.mryan.weather.service;

import com.mryan.weather.api.openstreetmap.OpenStreetMapClient;
import com.mryan.weather.api.openstreetmap.model.OpenStreetMapResponse;
import com.mryan.weather.domain.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CoordinateService {

    private final static String API_FORMAT = "json";
    private final static String CITY_TYPE = "city";

    @Autowired
    private OpenStreetMapClient openStreetMapClient;

    public City getCoordinates(String query) {
        List<OpenStreetMapResponse> apiResponse = openStreetMapClient.search(query, API_FORMAT).stream()
            .filter(r -> r.getType().equals(CITY_TYPE))
            .collect(Collectors.toList());

        //TODO: handle empty results
        OpenStreetMapResponse chosenCity = apiResponse.get(0);

        return City.builder()
            .query(query)
            .latitude(chosenCity.getLatitude())
            .longitude(chosenCity.getLongitude())
            .name(chosenCity.getName())
            .build();
    }

}
