package com.mryan.weather.forecast;

import com.mryan.weather.api.weathergov.WeatherGovClient;
import com.mryan.weather.api.weathergov.model.WeatherGovForecastPeriod;
import com.mryan.weather.domain.ForecastCity;
import com.mryan.weather.domain.ForecastCityDetail;
import com.mryan.weather.domain.ForecastResponse;
import com.mryan.weather.api.weathergov.model.WeatherGovForecastResponse;
import com.mryan.weather.api.weathergov.model.WeatherGovSearchResponse;
import com.mryan.weather.domain.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ForecastResponseBuilder {

    @Autowired
    private WeatherGovClient weatherGovClient;

    @Value("${forecast.indexes}")
    private List<Integer> forecaseIndexes;

    public ForecastResponse getForecastResponse(List<City> cities) {
        ForecastResponse forecastResponse = new ForecastResponse();
        List<ForecastCity> forecastCities = new ArrayList<>();

        for(City city : cities) {
            ForecastCity forecastCity = ForecastCity.builder()
                    .name(city.getName())
                    .detail(new ArrayList<>())
                    .build();

            //TODO: error handling - timeouts, retries (Feign?), caching
            String coordinates = city.getLatitude() + "," + city.getLongitude();
            WeatherGovSearchResponse apiSearchResponse = weatherGovClient.search(coordinates);

            String gridPoints = apiSearchResponse.getProperties().getGridX() + "," + apiSearchResponse.getProperties().getGridY();
            WeatherGovForecastResponse apiForecastResponse = weatherGovClient.getForecast(
                    apiSearchResponse.getProperties().getCwa(),
                    gridPoints
            );

            for(Integer periodIndex : forecaseIndexes) {
                //TODO: NPE!
                WeatherGovForecastPeriod period = apiForecastResponse.getProperties().getPeriods().get(periodIndex);
                forecastCity.getDetail().add(
                    ForecastCityDetail.builder()
                        .description(period.getShortForecast())
                        .startTime(period.getStartTime())
                        .endTime(period.getEndTime())
                        .build()
                );
            }

            forecastCities.add(forecastCity);
        }

        forecastResponse.setForecast(forecastCities);
        return forecastResponse;
    }

}
