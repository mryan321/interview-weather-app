package com.mryan.weather.api.weathergov.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class WeatherGovForecastPeriod {

    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss[XXX][X]")
    private LocalDateTime startTime;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss[XXX][X]")
    private LocalDateTime endTime;
    private String shortForecast;
    private String detailedForecast;

}
