package com.mryan.weather.api.weathergov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class WeatherGovForecastResponse {

    private WeatherGovForecastProperties properties;

}