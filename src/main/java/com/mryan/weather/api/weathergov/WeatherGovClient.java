package com.mryan.weather.api.weathergov;

import com.mryan.weather.api.weathergov.model.WeatherGovForecastResponse;
import com.mryan.weather.api.weathergov.model.WeatherGovSearchResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "weathergov-client", url = "${weathergov.url}")
public interface WeatherGovClient {

    @RequestMapping(method = RequestMethod.GET, value = "/points/{coordinates}")
    WeatherGovSearchResponse search(
        @PathVariable(value="coordinates") String coordinates
    );

    @RequestMapping(method = RequestMethod.GET, value = "/gridpoints/{code}/{gridpoints}/forecast")
    WeatherGovForecastResponse getForecast(
        @PathVariable(value="code") String code,
        @PathVariable(value="gridpoints") String gridPoints
    );

}
