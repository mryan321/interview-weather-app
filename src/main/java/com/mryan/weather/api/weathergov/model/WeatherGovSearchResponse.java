package com.mryan.weather.api.weathergov.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class WeatherGovSearchResponse {

    private String id;
    private WeatherGovSearchProperties properties;

}