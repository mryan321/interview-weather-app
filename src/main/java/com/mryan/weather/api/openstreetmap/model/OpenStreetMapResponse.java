package com.mryan.weather.api.openstreetmap.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class OpenStreetMapResponse {

    @JsonCreator
    public OpenStreetMapResponse(
        @JsonProperty("lat") Float latitude,
        @JsonProperty("lon") Float longitude,
        @JsonProperty("display_name") String name
    ){
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
    }

    private Float latitude;
    private Float longitude;
    private String name;
    private String type;

}
