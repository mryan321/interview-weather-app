package com.mryan.weather.api.openstreetmap;

import com.mryan.weather.api.openstreetmap.model.OpenStreetMapResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "openstreetmap-client", url = "${openstreetmap.url}")
public interface OpenStreetMapClient {

    @RequestMapping(method = RequestMethod.GET, value = "/search")
    List<OpenStreetMapResponse> search(
        @RequestParam(value="q") String query,
        @RequestParam(value="format") String format
    );

}