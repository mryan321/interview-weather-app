package com.mryan.weather.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Detail is the object: properties.forecast (need an extra http call)
 *  --> https://api.weather.gov/points/40.7143,-74.006
 */
@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class ForecastCity {
    private String name;
    private List<ForecastCityDetail> detail;
}