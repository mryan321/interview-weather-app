package com.mryan.weather.domain;

import java.util.List;

public class ForecastResponse {
    private List<ForecastCity> forecast;

    public List<ForecastCity> getForecast() {
        return forecast;
    }

    public void setForecast(List<ForecastCity> forecast) {
        this.forecast = forecast;
    }
}