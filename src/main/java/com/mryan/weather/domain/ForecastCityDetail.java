package com.mryan.weather.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Details are present here
 * https://api.weather.gov/gridpoints/OKX/32,34/forecast
 * object: properties.periods[*].{startTime, endTime, detailedForecast}
 * periods example: https://gist.github.com/db80/e19cab344d37b918bbf81d3a27031c59
 */
@Data
@Builder(toBuilder=true)
@NoArgsConstructor
@AllArgsConstructor
public class ForecastCityDetail {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String description;
}
