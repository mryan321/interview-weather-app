package com.mryan.weather.controller;

import com.mryan.weather.domain.ForecastResponse;
import com.mryan.weather.service.ForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
public class WeatherController {

    @Autowired
    private ForecastService forecastService;

    @GetMapping("/weather")
    public ForecastResponse get(@RequestParam(value="city", required=false) List<String> cities) {
        return forecastService.getForecast(cities);
    }
}
