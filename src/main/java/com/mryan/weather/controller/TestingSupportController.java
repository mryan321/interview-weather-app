package com.mryan.weather.controller;


import com.mryan.weather.api.openstreetmap.OpenStreetMapClient;
import com.mryan.weather.api.openstreetmap.model.OpenStreetMapResponse;
import com.mryan.weather.api.weathergov.WeatherGovClient;
import com.mryan.weather.api.weathergov.model.WeatherGovForecastResponse;
import com.mryan.weather.api.weathergov.model.WeatherGovSearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(
    path = "/testing-support",
    produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
@ConditionalOnProperty("test-support.enabled")
public class TestingSupportController {

    @Autowired
    private OpenStreetMapClient openStreetMapClient;

    @Autowired
    private WeatherGovClient weatherGovClient;

    @GetMapping("/osm")
    public List<OpenStreetMapResponse> getOpenStreetMapResponse() {
        return openStreetMapClient.search("new york, usa", "json");
    }

    @GetMapping("/w")
    public WeatherGovSearchResponse getWeatherGovReponse() {
        return weatherGovClient.search("40.71427,-74.00597");
    }

    @GetMapping("/f")
    public WeatherGovForecastResponse getWeatherGovForecastReponse() {
        return weatherGovClient.getForecast("OKX", "32,34");
    }
}
